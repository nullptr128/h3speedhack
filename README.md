This simple mod doubles Heroes 3 game speed.

It is compatabile with ERA. In order to use it, simply put speedhack.dll into your EraPlugins folder.

I am not the author of speedhack, original sources are here:
http://forum.cheatengine.org/files/speedhacksrc_152.zip

I've only finetuned it to work with FreePascal.